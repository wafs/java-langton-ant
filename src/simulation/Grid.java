package simulation;

/**
 * 
 * Grid class
 *
 * Container class to hold the grid information.
 * This class should be a wrapper over the two dimensional array of the board.
 * 
 * @author _________
 *
 */

public class Grid {
	
	int height,
		width;

	boolean[][] grid;

	/**
	 * Constructor for the grid.
	 * 
	 * Initial state is that the every cell in the grid is white
	 * 
	 * @param height - height of the grid
	 * @param width - width of the grid
	 */
	public Grid(int height, int width) {
		this.height = height;
		this.width = width;
		grid = new boolean[height][width];
		
		for(int i = 0; i < grid.length; i++){
			for(int j = 0; j < grid[0].length; j++){
				grid[i][j] = true;
			}
		}
	}
	
	/**
	 * Alternate constructor for the grid.
	 * <br/>
	 * Takes in the a boolean grid array and copies the values to create a deep copy.
	 * 
	 * @param contents - boolean grid
	 */
	public Grid(boolean[][] contents) {
		this.height = contents.length;
		this.width = contents[0].length;
		grid = new boolean[height][width];
		
		for(int i = 0; i < this.height; i++){
			for(int j = 0; j < this.width; j++){
				grid[i][j] = contents[i][j];
			}
		}		
		
	}

	public int getHeight() {
		return this.height;
	}

	public int getWidth() {
		return this.width;
	}

	public boolean isWhite(int i, int j) {
		return grid[i][j];
	}

	protected void setWhite(int i, int j) {
		try {
			grid[i][j] = true;
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("SETWHITE >> Position: " + i + " , " + j + " is out of bounds");
		}
	}

	protected void setBlack(int i, int j) {
		try {
			grid[i][j] = false;
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("SETBLACK >> Position: " + i + " , " + j + " is out of bounds");
		}
	}
}