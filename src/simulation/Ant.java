package simulation;

import core.Direction;

/**
 * 
 * Ant class
 * 
 * Container class to hold the information about the ant.
 * 
 * @author Wafik Salim
 * 
 *  *
 */
public class Ant {
	
	private int iPos,
				jPos;
	
	private Direction direction;
	
	public Ant(int iPos, int jPos, Direction direction){
		this.iPos = iPos;
		this.jPos = jPos;
		this.direction = direction;
	}
	
	public int getIPos() {
		return iPos;
	}
	
	protected void setIPos(int iPos) {
		try {
			this.iPos = iPos;
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("iPos out of bounds");
		}
	}
	
	public int getJPos() {
		return jPos;
		
	}
	protected void setJPos(int jPos) {
		try {
			this.jPos = jPos;
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("jPos out of bounds");
		}
	}
	
	public Direction getDirection() {
		return direction;
	}
	
	protected void setDirection(Direction direction) {
		this.direction = direction;
	}
	
}
