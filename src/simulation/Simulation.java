package simulation;

import core.Direction;

/**
 * 
 * Simulation class
 * 
 * @author Wafik Salim
 * 
 */
public class Simulation {
	
	int height, 
		width, 
		maxTimeSteps, 
		currentTimeStep, 
		antI, 
		antJ;

	Direction antDir;

	Ant ant;

	Grid grid;

	/**
	 * Takes instance variables for the program to use.
	 * 
	 * @param height
	 *            - the height of the grid
	 * @param width
	 *            - the width of the grid
	 * @param antStartI
	 *            - the original I coordinate of the ant
	 * @param antStartJ
	 *            - the original J coordinate of the ant
	 * @param originalDirection
	 *            - the original direction the ant is facing
	 * @param maxTimeSteps
	 * 			  - the maximum amount of iterations
	 */
	public Simulation(int height, int width, int antStartI, int antStartJ,
			Direction originalDirection, int maxTimeSteps) {

		this.maxTimeSteps = maxTimeSteps;

		this.grid = new Grid(height, width);
		this.ant = new Ant(antStartI, antStartJ, originalDirection);

	}
	
	/**
	 * Checks if the ant is out of bounds at the current position the method is called.
	 * @return
	 * 		- True if out of bounds, otherwise False.
	 */
	public boolean CheckOOB() {
		Ant a = this.ant;
		Grid g = this.grid;
		
		if (a.getJPos() < 0 || a.getJPos() > g.width - 1 || a.getIPos() < 0
				|| a.getIPos() > g.height - 1) {
			return true;
		} else {
			return false;
		}
		
	}

	/**
	 * Execute a time step for the simulation.
	 * <br/>
	 * If the ant is in a valid location, it moves it 1 place.
	 * <br/>
	 * If the ant is in a valid position after movement, it then rotates the ant and changes the tile colour. 
	 * <br/>
	 * If the simulation is completed, it does nothing.
	 * 
	 */

	public void executeStep() {
		
		while(isCompleted() == false){
			antI = ant.getIPos();
			antJ = ant.getJPos();
			antDir = ant.getDirection();
			
			if (CheckOOB()) {
				break;
			} else {
				
			// Movement ~ Placement of Ant
			switch (antDir) {
			case NORTH:
				ant.setIPos(antI - 1);
				antI = ant.getIPos();
				break;
			case SOUTH:
				ant.setIPos(antI + 1);
				antI = ant.getIPos();
				break;
			case EAST:
				ant.setJPos(antJ + 1);
				antJ = ant.getJPos();
				break;
			case WEST:
				ant.setJPos(antJ - 1);
				antJ = ant.getJPos();
				break;
			}
			
			if (CheckOOB()) {
				break;
			}else{
				// Rotation of Ant
				// Setting tile colour
				if (grid.isWhite(antI, antJ)) {
					Rotate(antDir, "left");
					grid.setBlack(antI, antJ);
				} else {
					Rotate(antDir, "right");
					grid.setWhite(antI, antJ);
				}
	
			++currentTimeStep;
				}
			}
			break;
		}

	}
	
	/**
	 * Rotate direction helper method. The original enum was rearranged to make this
	 * work. <br/></br>Going left takes it "up" the ordinal list, which is same as
	 * rotating -90 degrees & right takes it "down" or +90.<br/> If it goes too far left / right, it
	 * just selects the opposite end of the enum list for "circular" rotation.
	 */
	public void Rotate(Direction direction, String rotation) {
		
		//retrieve integer form of enum
		int currentOrdinal = direction.ordinal();

		
		if (rotation.equals("left")) {
			currentOrdinal--;
			if (currentOrdinal < 0) {
				currentOrdinal = Direction.values().length - 1;
			}

		}

		if (rotation.equals("right")) {
			currentOrdinal++;
			if (currentOrdinal > Direction.values().length - 1) {
				currentOrdinal = 0;
			}
		}
		
		// convert int back into enum, and use it to set direction
		this.ant.setDirection(Direction.values()[currentOrdinal]);
	}

	/**
	 * Method to check if the simulation is completed.
	 * 
	 * The simulation is completed if and only if: <ul><li> it has reached the maximum
	 * time steps allowed <li> the ant has moved off the grid</ul>
	 * 
	 * @return true - the simulation is completed
	 * @return false - the simulation is not completed
	 */
	public boolean isCompleted() {
		if (CheckOOB() || currentTimeStep >= maxTimeSteps) {
			System.out.println("isCompleted returning true");
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Method to return a copy of the current grid.
	 * 
	 * You should always return a copy of an object if you do not want your base
	 * object to be changed by any code calling this method.
	 * 
	 * @return a copy of the grid.
	 */
	public Grid getCopyOfGrid() {
		boolean[][] gridContents = grid.grid;

		Grid copy = new Grid(gridContents);

		return copy;
	}

	/**
	 * Method to return a copy of the current ant.
	 * 
	 * You should always return a copy of an object if you do not want your base
	 * object to be changed by any code calling this method.
	 * 
	 * @return a copy of the ant.
	 */
	public Ant getCopyOfAnt() {
		antI = ant.getIPos();
		antJ = ant.getJPos();
		antDir = ant.getDirection();

		Ant copy = new Ant(antI, antJ, antDir);
		return copy;
	}
}