package core;

public enum Direction {
	NORTH, EAST, SOUTH, WEST;
}